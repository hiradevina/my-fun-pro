divisor n = [x | x <- [1 .. n] , n `mod` x == 0]

quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs , y <= x] ++ [x] ++ quickSort [y | y <- xs, y > x]

perm [] = [[]]
perm ls = [x : sisa | x <- ls , sisa <- perm (ls \\ [x])] -- difference



pythaTriple n = [ (x,y,z) | z <- [1..n], y <- [1..z], x <- [1..y], x^2 + y^2 == z^2]