-- nomor 1
data Tree a = Leaf a | Branch (Tree a) (Tree a)

mapTree :: (a->b) -> Tree a -> Tree b

mapTree f (Leaf x)       = Leaf (f x)
mapTree f (Branch t1 t2) = Branch (mapTree f t1) 
                                   (mapTree f t2)

foldtl f z (Leaf x)       = f z x
foldtl f z (Branch t1 t2) = foldtl f (f z t1) t2

foldtr f z (Leaf x)       = f z x
foldtr f z (Branch t1 t2) = f t1 (foldtr f z t2)

-- nomor 2
data Expr = C Float | Add Expr Expr | Sub Expr Expr | Mul Expr Expr | Div Expr Expr | Let String Expr Expr
data Expr = C Float | Expr :+ Expr | Expr :- Expr | Expr :* Expr | Expr :/ Expr | V [Char] | Let String Expr Expr deriving Show

Add :: Expr -> Expr -> Expr
Add (C c1) (C c2) = C (c1 + c2)

Sub :: Expr -> Expr -> Expr
Sub (C c1) (C c2) = C (c1 - c2)

Mult :: Expr -> Expr -> Expr
Mult (C c1) (C c2) = C (c1 * c2)

Div :: Expr -> Expr -> Expr
Div (C c1) (C c2) = C (c1 / c2)

subst :: String -> Expr -> Expr -> Expr
subst v0 e0 (V v1)         = if (v0 == v1) then e0 else (V v1)
subst v0 e0 (C c)          = (C c)
subst v0 e0 (e1 :+ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :* e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :/ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2)

evaluate :: Expr -> Float
evaluate (C x) = x
evaluate (e1 :+ e2)    = evaluate e1 + evaluate e2
evaluate (e1 :- e2)    = evaluate e1 - evaluate e2
evaluate (e1 :* e2)    = evaluate e1 * evaluate e2
evaluate (e1 :/ e2)    = evaluate e1 / evaluate e2
evaluate (Let v e0 e1) = evaluate (subst v e0 e1)
evaluate (V v)         = 0.0

-- evaluateHOF :: Expr -> Op -> Expr -> Float
-- evaluateHOF (C c1) = c1
-- evaluateHOF (C c1) Add (C c2) = c1 + c2
-- evaluateHOF (C c1) Sub (C c2) = c1 - c2
-- evaluateHOF (C c1) Mul (C c2) = c1 * c2
-- evaluateHOF (C c1) Div (C c2) = c1 / c2
-- evaluateHOF (C c1) Add (C c2) = evaluate(c1) + evaluate(c2)

evaluateFold (add, sub, mult, div) (C c) = c
evaluateFold (add, sub, mult, div) (e1 :+ e2 ) = add (evaluateFold (add, sub, mult, div) e1) (evaluateFold (add, sub, mult, div) e2)
evaluateFold (add, sub, mult, div) (e1 :- e2 ) = sub (evaluateFold (add, sub, mult, div) e1) (evaluateFold (add, sub, mult, div) e2)
evaluateFold (add, sub, mult, div) (e1 :* e2 ) = mult (evaluateFold (add, sub, mult, div) e1) (evaluateFold (add, sub, mult, div) e2)
evaluateFold (add, sub, mult, div) (e1 :/ e2 ) = div (evaluateFold (add, sub, mult, div) e1) (evaluateFold (add, sub, mult, div) e2)

newEvaluate expr = evaluateFold (add, sub, mult, div)
    where add = (+)
          sub = (-)
          mult = (*)
          div = (/)

