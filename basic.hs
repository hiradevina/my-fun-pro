-- source: http://learnyouahaskell.com

successor = succ 8 -- 8 + 1

-- We can use the let keyword to 
-- define a name right in GHCI. 
-- Doing let a = 1 inside GHCI is the equivalent 
-- of writing a = 1 in a script and then loading it.
let lostNumbers = [4,8,15,16,23,42]

-- use ++, Haskell has to walk through the whole list on the left side of ++
appendList = [1,2,3,4] ++ [9,10,11,12] 
-- return [1,2,3,4,9,10,11,12]
appendString = ['w','o'] ++ ['o','t'] 
-- return 'woot'

appendFromFront = 'A':" SMALL CAT"
-- ditaro di depan, jadinya 'A SMALL CAT', kayak append tapi boleh nerima gak list

compareList = [3,2,4] < [100, 5]
-- return True, karena bandingin head nya aja

-- head takes a list and returns its head. The head of a list is basically its first element.
tryHead = head [5,4,3,2,1] 
-- return 5 (balikinnya itemnya, gak dalam list)

-- tail takes a list and returns its tail. In other words, it chops off a list's head.
tryTail = tail [5,4,3,2,1] 
-- return [4,3,2,1] (balikin dlm bentuk List)

-- last takes a list and returns its last element.
tryLast = last [5,4,3,2,1] 
-- return 1 (balikinnya itemnya)

-- init takes a list and returns everything except its last element.
tryInit = init [5,4,3,2,1]
-- return [5,4,3,2] (balikinnya list)

-- take (ambil paling depan) vs drop (ambil paling belakang)

-- list comprehention
boomBangs xs = [ if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x]
-- manggilnya boomBangs [7 .. 10] gitu

list2 = [ x*y | x <- [2,5,10], y <- [8,10,11]]
-- 2*8 trs 2*10 dst, pokoknya selalu dari kiri (yg 2 5 10)

length' xs = sum [1 | _ <- xs]
-- hasil dari list comprehension di sum (karena isinya cuma 1 jadinya sama kayak length xs nya)

removeNonUppercase st = [ c | c <- st, c `elem` ['A'..'Z']] 
-- testnya removeNonUppercase "Hahaha! Ahahaha!" nnt returnnya "HA"


-- TUPLE --
testFst = fst (8,11) -- returnnya 8 (ambil first component)
testSnd = snd (8,11) -- kebalikan fst

-- ini jadi cuma yan 5 3 2 yg pertama aja, soalnya pasangannya cuma 3 juga
testZip = zip [5,3,2,6,2,7,2,5,4,6,6] ["im","a","turtle"]